;(function ($) {
	$('a[href^="#"]').on('click', function (event) {
		$('a[href^="#"]').removeClass('active')
		$(this).addClass('active')
		var target = $($(this).attr('href'))

		if (target.length) {
			event.preventDefault()
			$('html, body').animate(
				{
					scrollTop: target.offset().top,
				},
				800
			)
		}
	})
	$('.mobile__menu--icon').click(function () {
		$('.mobile__menu--icon').toggleClass('open')
		$('.menu-container').toggleClass('menu-toggle')
	})

	$('.search__icon--btn').click(function (event) {
		$('.header__search .search__wrapper').toggleClass('search--hide')
		$(this).parent().find('input')[0].focus()
	})

	$('.search__advance').click(function () {
		$('.search__advance + .search__options').toggle()
	})
	$('#resources-owl').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 991,
				settings: {
					dots: true,
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		],
	})
	$('#lastnews-owl').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			// {
			// 	breakpoint: 1200,
			// 	settings: {
			// 		slidesToShow: 3,
			// 		slidesToScroll: 1,
			// 		infinite: true,
			// 		dots: true,
			// 	},
			// },
			{
				breakpoint: 991,
				settings: {
					dots: true,
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		],
	})

	$('#category-owl').slick({
		dots: true,
		//	dotsClass: 'slick-dots-custom',
		infinite: true,
		speed: 300,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		appendArrows: '.slick-arrows-custom',
		nextArrow:
			'<div class="slick-arrow-custom" aria-label="Next">Next category<span class="slick-next-img"></span></div>',
		prevArrow:
			'<div class="slick-arrow-custom" aria-label="Previous"><span class="slick-prev-img"></span>Previous category</div>',
	})
})(jQuery)
